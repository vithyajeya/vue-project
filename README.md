# Vue project
Clone or download as .zip package(and unpack) the repo first.

## Some Vue Basics with Juypiter Notebooks
To show some of the basics of vue, jupyter notebooks(.ipynb) are going to be used. So jupyter environment should be installed on your host machine. 
**If you do not want to install the jupyter environment you can open the index.html in the browser and for modifying the files(index.html and app.js) your favorite text editor of choice.**
 
**You will find step by step solutions we a going through the session in the 0* folders.** 


## Run a tiny vue web application
To run a vue web application a docker image is been created. During the live session some files, which are created in the container are going to be edited. To make the editing easy the **dockerhome** folder is been shared between host and the container.   

### Run docker container
Run the command in the same folder where the **dockerhome** folder is located
**linux user**
```
docker run -it -d -p 9999:8080 -v $PWD/dockerhome:/home/node --name vue_project vithyajeya/vue_cli_nginx:v1.0.0
```
**windows users**
Please accept the file sharing dialog with 'share it'!   
```
docker run -it -d -p 9999:8080 -v %CD%\dockerhome:/home/node --name vue_project vithyajeya/vue_cli_nginx:v1.0.0
```
### Run shell
```
docker exec -it vue_project /bin/sh
``` 
