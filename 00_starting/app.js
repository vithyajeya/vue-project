var persons = ["Putin,", "Biden,", "Obama", "Mexico,", "Ryan Gosling", "The Democrats"];
var part1 = ["no talent,", "on the way down,", "nasty tone,", "looking like a fool,", "bad hombre,"];
var part2 = ["got destroyed by my ratings.", "rigged the election.", "will pay for the wall."];
var part3 = ["So sad", "Apologize", "So true", "Media won't report", "Big trouble", "Fantastic job"];

var sentenceGroup = [persons, part1, part2, part3];

var button = document.querySelector("#my-app button");
var news = document.querySelector("#my-app h2");

var generator = function() {
    var sentence = '';
    for(var i = 0; i < sentenceGroup.length; i++) {
        var sentencePart = sentenceGroup[i];
        sentence += sentencePart[Math.floor(Math.random()*sentencePart.length)] + ' ';
    }
    news.innerHTML = sentence;
}

button.addEventListener('click', generator);
